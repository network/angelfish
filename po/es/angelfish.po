# Spanish translations for angelfish.po package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-angelfish package.
#
# Automatically generated, 2019.
# SPDX-FileCopyrightText: 2019, 2020, 2021, 2022, 2023, 2024, 2025 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: angelfish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-03-08 00:43+0000\n"
"PO-Revision-Date: 2025-01-24 14:03+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.12.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ecuadra@eloihr.net"

#: angelfish-webapp/main.cpp:71
#, kde-format
msgid "desktop file to open"
msgstr "archivo de escritorio a abrir"

#: angelfish-webapp/main.cpp:98
#, kde-format
msgid "Angelfish Web App runtime"
msgstr "Entorno en tiempo de ejecución para la aplicación web Angelfish"

#: angelfish-webapp/main.cpp:100
#, kde-format
msgid "Copyright 2020 Angelfish developers"
msgstr "Copyright 2020 por los desarrolladores de Angelfish"

#: angelfish-webapp/main.cpp:102
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: lib/angelfishwebprofile.cpp:63
#, kde-format
msgid "Download finished"
msgstr "Descarga terminada"

#: lib/angelfishwebprofile.cpp:67
#, kde-format
msgid "Download failed"
msgstr "La descarga ha fallado"

#: lib/angelfishwebprofile.cpp:88
#, kde-format
msgid "Open"
msgstr "Abrir"

#: lib/contents/ui/AuthSheet.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Authentication Required"
msgstr "Se necesita autenticación"

#: lib/contents/ui/AuthSheet.qml:30
#, kde-format
msgctxt "@label:textbox"
msgid "Username:"
msgstr "Nombre de usuario:"

#: lib/contents/ui/AuthSheet.qml:37
#, kde-format
msgctxt "@label:textbox"
msgid "Password:"
msgstr "Contraseña:"

#: lib/contents/ui/DownloadQuestion.qml:16
#, fuzzy, kde-format
#| msgid "Do you want to download this file?"
msgctxt "Would you like to download (filename) from (domain)?"
msgid "Would you like to download %1 from %2?"
msgstr "¿Desea descargar este archivo?"

#: lib/contents/ui/DownloadQuestion.qml:25
#: src/contents/ui/AdblockFilterDownloadQuestion.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Download"
msgstr "Descargar"

#: lib/contents/ui/DownloadQuestion.qml:33 lib/contents/ui/PrintPreview.qml:163
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancelar"

#: lib/contents/ui/ErrorHandler.qml:47
#, kde-format
msgctxt "@title"
msgid "Error Loading the Page"
msgstr "Ha ocurrido un error al cargar la página"

#: lib/contents/ui/ErrorHandler.qml:58
#, kde-format
msgctxt "@action:intoolbar"
msgid "Retry"
msgstr "Volver a intentar"

#: lib/contents/ui/ErrorHandler.qml:74
#, kde-format
msgid ""
"Do you wish to continue?\n"
"\n"
" If you wish so, you may continue with an unverified certificate.\n"
" Accepting an unverified certificate means you may not be connected with the "
"host you tried to connect to.\n"
"\n"
" Do you wish to override the security check and continue?"
msgstr ""
"¿Desea continuar?\n"
"\n"
"Si así lo desea, puede continuar con un certificado sin verificar.\n"
"Aceptar un certificado sin verificar significa que es posible que no esté "
"conectado a la máquina a la que ha intentado conectarse.\n"
"\n"
"¿Desea anular la comprobación de seguridad y continuar?"

#: lib/contents/ui/ErrorHandler.qml:81
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr "Sí"

#: lib/contents/ui/ErrorHandler.qml:90
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr "No"

#: lib/contents/ui/JavaScriptDialogSheet.qml:49
#, kde-format
msgctxt "@title:window"
msgid "This Page Says"
msgstr "Esta página dice"

#: lib/contents/ui/JavaScriptDialogSheet.qml:64
#, kde-format
msgid ""
"The website asks for confirmation that you want to leave. Unsaved "
"information might not be saved."
msgstr ""
"El sitio web solicita confirmación de que quiere abandonar la página. Es "
"posible que exista información sin guardar."

#: lib/contents/ui/JavaScriptDialogSheet.qml:79
#, kde-format
msgctxt "@action:button"
msgid "Leave Page"
msgstr "Abandonar la página"

#: lib/contents/ui/JavaScriptDialogSheet.qml:87
#, kde-format
msgctxt "@action:button"
msgid "Submit"
msgstr "Enviar"

#: lib/contents/ui/PermissionQuestion.qml:22
#, kde-format
msgid ""
"Do you want to allow the website <b>%1</b> to access the <b>microphone</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda acceder al <b>micrófono</b>?"

#: lib/contents/ui/PermissionQuestion.qml:24
#, kde-format
msgid "Do you want to allow the website <b>%1</b> to access the <b>camera</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda acceder a la <b>cámara</b>?"

#: lib/contents/ui/PermissionQuestion.qml:26
#, kde-format
msgid ""
"Do you want to allow the website <b>%1</b> to access the <b>camera and the "
"microphone</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda acceder a la <b>cámara y al "
"micrófono</b>?"

#: lib/contents/ui/PermissionQuestion.qml:28
#, kde-format
msgid "Do you want to allow the website <b>%1</b> to share your <b>screen</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda compartir su <b>pantalla</"
"b>?"

#: lib/contents/ui/PermissionQuestion.qml:30
#, kde-format
msgid ""
"Do you want to allow the website <b>%1</b> to share the sound <b>output</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda compartir la <b>salida de "
"sonido</b>?"

#: lib/contents/ui/PermissionQuestion.qml:32
#, kde-format
msgid ""
"Do you want to allow the website <b>%1</b> to send you <b>notifications</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda enviarle <b>notificaciones</"
"b>?"

#: lib/contents/ui/PermissionQuestion.qml:34
#, kde-format
msgid ""
"Do you want to allow the website <b>%1</b> to access the <b>geo location</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda acceder a la "
"<b>geolocalización</b>?"

#: lib/contents/ui/PermissionQuestion.qml:36
#, kde-format
msgid ""
"Do you want to allow the website <b>%1</b> to access the <b>clipboard</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda acceder al <b>portapapeles</"
"b>?"

#: lib/contents/ui/PermissionQuestion.qml:38
#, kde-format
msgid "Do you want to allow the website <b>%1</b> to access your <b>fonts</b>?"
msgstr ""
"¿Desea permitir que el sitio web <b>%1</b> pueda acceder a los <b>tipos de "
"letra</b>?"

#: lib/contents/ui/PermissionQuestion.qml:40
#, kde-format
msgctxt ""
"The website (website) requested an unknown permission: Do you want to allow "
"the website (website) to access your fonts?"
msgid "The website %1 requested an unknown permission: %2"
msgstr "El sitio web %1 ha solicitado un permiso desconocido: %2"

#: lib/contents/ui/PermissionQuestion.qml:48
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "Aceptar"

#: lib/contents/ui/PermissionQuestion.qml:58
#, kde-format
msgctxt "@action:button"
msgid "Decline"
msgstr "Declinar"

#: lib/contents/ui/PrintPreview.qml:24
#, kde-format
msgctxt "@title:window"
msgid "Print"
msgstr "Imprimir"

#: lib/contents/ui/PrintPreview.qml:64
#, kde-format
msgctxt "@title:group"
msgid "Destination"
msgstr "Destino"

#: lib/contents/ui/PrintPreview.qml:68
#, kde-format
msgctxt "@title:group"
msgid "Save to PDF"
msgstr "Guardar en PDF"

#: lib/contents/ui/PrintPreview.qml:74
#, kde-format
msgctxt "@title:group"
msgid "Orientation"
msgstr "Orientación"

#: lib/contents/ui/PrintPreview.qml:99
#, kde-format
msgctxt "@title:group"
msgid "Paper Size"
msgstr "Tamaño del papel"

#: lib/contents/ui/PrintPreview.qml:145
#, kde-format
msgctxt "@title:group"
msgid "Options"
msgstr "Opciones"

#: lib/contents/ui/PrintPreview.qml:149
#, kde-format
msgctxt "@label:checkbox"
msgid "Print backgrounds"
msgstr "Imprimir fondos"

#: lib/contents/ui/PrintPreview.qml:168
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Guardar"

#: lib/contents/ui/WebView.qml:243
#, kde-format
msgctxt "@info:status"
msgid "Website was opened in a new tab"
msgstr "El sitio web se ha abierto en una nueva pestaña"

#: lib/contents/ui/WebView.qml:261
#, kde-format
msgctxt "@info:status"
msgid "Entered Full Screen mode"
msgstr "Ha entrado en el modo a pantalla completa"

#: lib/contents/ui/WebView.qml:262
#, kde-format
msgctxt "@action:button"
msgid "Exit Full Screen (Esc)"
msgstr "Salir de pantalla completa (Esc)"

#: lib/contents/ui/WebView.qml:395
#, kde-format
msgctxt "@action:inmenu"
msgid "Play"
msgstr "Reproducir"

#: lib/contents/ui/WebView.qml:396
#, kde-format
msgctxt "@action:inmenu"
msgid "Pause"
msgstr "Pausar"

#: lib/contents/ui/WebView.qml:403
#, kde-format
msgctxt "@action:inmenu"
msgid "Unmute"
msgstr "Reanudar el sonido"

#: lib/contents/ui/WebView.qml:404
#, kde-format
msgctxt "@action:inmenu"
msgid "Mute"
msgstr "Silenciar"

#: lib/contents/ui/WebView.qml:414
#, kde-format
msgctxt "@label"
msgid "Speed"
msgstr "Velocidad"

#: lib/contents/ui/WebView.qml:437
#, kde-format
msgctxt "@action:inmenu"
msgid "Loop"
msgstr "Bucle"

#: lib/contents/ui/WebView.qml:444
#, kde-format
msgctxt "@action:inmenu"
msgid "Exit Fullscreen"
msgstr "Salir de pantalla completa"

#: lib/contents/ui/WebView.qml:444
#, kde-format
msgctxt "@action:inmenu"
msgid "Enter Fullscreen"
msgstr "Entrar en pantalla completa"

#: lib/contents/ui/WebView.qml:457
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Controls"
msgstr "Ocultar controles"

#: lib/contents/ui/WebView.qml:458
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Controls"
msgstr "Mostrar controles"

#: lib/contents/ui/WebView.qml:466
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Video"
msgstr "Abrir vídeo"

#: lib/contents/ui/WebView.qml:466
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Audio"
msgstr "Abrir sonido"

#: lib/contents/ui/WebView.qml:467
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Video in New Tab"
msgstr "Abrir vídeo en nueva pestaña"

#: lib/contents/ui/WebView.qml:467
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Audio in New Tab"
msgstr "Abrir sonido en nueva pestaña"

#: lib/contents/ui/WebView.qml:479
#, kde-format
msgctxt "@action:inmenu"
msgid "Save Video"
msgstr "Guardar vídeo"

#: lib/contents/ui/WebView.qml:485
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Video Link"
msgstr "Copiar enlace del vídeo"

#: lib/contents/ui/WebView.qml:491
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Image"
msgstr "Abrir imagen"

#: lib/contents/ui/WebView.qml:491
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Image in New Tab"
msgstr "Abrir imagen en nueva pestaña"

#: lib/contents/ui/WebView.qml:503
#, kde-format
msgctxt "@action:inmenu"
msgid "Save Image"
msgstr "Guardar imagen"

#: lib/contents/ui/WebView.qml:509
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Image"
msgstr "Copiar imagen"

#: lib/contents/ui/WebView.qml:515
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Image Link"
msgstr "Copiar enlace de la imagen"

#: lib/contents/ui/WebView.qml:521
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Link"
msgstr "Abrir enlace"

#: lib/contents/ui/WebView.qml:521
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Link in New Tab"
msgstr "Abrir enlace en nueva pestaña"

#: lib/contents/ui/WebView.qml:533
#, kde-format
msgctxt "@action:inmenu"
msgid "Bookmark Link"
msgstr "Marcar enlace"

#: lib/contents/ui/WebView.qml:545
#, kde-format
msgctxt "@action:inmenu"
msgid "Save Link"
msgstr "Guardar enlace"

#: lib/contents/ui/WebView.qml:551
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Link"
msgstr "Copiar enlace"

#: lib/contents/ui/WebView.qml:558
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy"
msgstr "Copiar"

#: lib/contents/ui/WebView.qml:564
#, kde-format
msgctxt "@action:inmenu"
msgid "Cut"
msgstr "Cortar"

#: lib/contents/ui/WebView.qml:570
#, kde-format
msgctxt "@action:inmenu"
msgid "Paste"
msgstr "Pegar"

#: lib/contents/ui/WebView.qml:578
#, kde-format
msgctxt "@action:inmenu"
msgid "Search for “%1”"
msgstr "Buscar «%1»"

#: lib/contents/ui/WebView.qml:591 src/contents/ui/desktop/desktop.qml:345
#: src/contents/ui/mobile.qml:296
#, kde-format
msgctxt "@action:inmenu"
msgid "Share Page"
msgstr "Compartir página"

#: lib/contents/ui/WebView.qml:603
#, kde-format
msgctxt "@action:inmenu"
msgid "View Page Source"
msgstr "Ver código fuente de la página"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:14
#, kde-format
msgctxt "@info"
msgid ""
"The adblocker is missing its filter lists, do you want to download them now?"
msgstr ""
"El bloqueador de publicidad carece de listas de filtrado. ¿Desea "
"descargarlas ahora?"

#: src/contents/ui/AdblockFilterDownloadQuestion.qml:35
#, kde-format
msgctxt "@info"
msgid "Downloading…"
msgstr "Descargando..."

#: src/contents/ui/Bookmarks.qml:12
#, kde-format
msgctxt "@title:window"
msgid "Bookmarks"
msgstr "Marcadores"

#: src/contents/ui/desktop/BookmarksPage.qml:30
#, kde-format
msgctxt "@info:placeholder"
msgid "Search bookmarks…"
msgstr "Buscar en los marcadores..."

#: src/contents/ui/desktop/BookmarksPage.qml:77
#, kde-format
msgctxt "@info:placeholder"
msgid "No bookmarks yet"
msgstr "Aún no hay marcadores"

#: src/contents/ui/desktop/desktop.qml:125
#, kde-format
msgctxt "@info:placeholder"
msgid "Search or enter URL…"
msgstr "Buscar o introducir URL..."

#: src/contents/ui/desktop/desktop.qml:249 src/contents/ui/desktop/Tabs.qml:355
#: src/contents/ui/Navigation.qml:437
#, kde-format
msgctxt "@action:inmenu"
msgid "New Tab"
msgstr "Nueva pestaña"

#: src/contents/ui/desktop/desktop.qml:256 src/contents/ui/mobile.qml:73
#, kde-format
msgctxt "@action:inmenu"
msgid "Leave Private Mode"
msgstr "Abandonar el modo privado"

#: src/contents/ui/desktop/desktop.qml:256 src/contents/ui/mobile.qml:73
#, kde-format
msgctxt "@action:inmenu"
msgid "Private Mode"
msgstr "Modo privado"

#: src/contents/ui/desktop/desktop.qml:265 src/contents/ui/mobile.qml:89
#, kde-format
msgctxt "@action:inmenu"
msgid "History"
msgstr "Historial"

#: src/contents/ui/desktop/desktop.qml:274 src/contents/ui/mobile.qml:81
#, kde-format
msgctxt "@action:inmenu"
msgid "Bookmarks"
msgstr "Marcadores"

#: src/contents/ui/desktop/desktop.qml:283 src/contents/ui/mobile.qml:93
#, kde-format
msgctxt "@action:inmenu"
msgid "Downloads"
msgstr "Descargas"

#: src/contents/ui/desktop/desktop.qml:296
#, kde-format
msgctxt "@action:inmenu"
msgid "Print"
msgstr "Imprimir"

#: src/contents/ui/desktop/desktop.qml:302
#, kde-format
msgctxt "@action:inmenu"
msgid "Full Screen"
msgstr "Pantalla completa"

#: src/contents/ui/desktop/desktop.qml:319
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Developer Tools"
msgstr "Ocultar las herramientas del desarrollador"

#: src/contents/ui/desktop/desktop.qml:320 src/contents/ui/mobile.qml:392
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Developer Tools"
msgstr "Mostrar las herramientas del desarrollador"

#: src/contents/ui/desktop/desktop.qml:329 src/contents/ui/mobile.qml:292
#, kde-format
msgctxt "@action:inmenu"
msgid "Find in Page"
msgstr "Buscar en la página"

#: src/contents/ui/desktop/desktop.qml:338 src/contents/ui/mobile.qml:375
#, kde-format
msgctxt "@action:inmenu"
msgid "Reader Mode"
msgstr "Modo lector"

#: src/contents/ui/desktop/desktop.qml:355
#, kde-format
msgctxt "@action:inmenu"
msgid "Add to Application Launcher"
msgstr "Añadir al lanzador de aplicaciones"

#: src/contents/ui/desktop/desktop.qml:374 src/contents/ui/mobile.qml:101
#, kde-format
msgctxt "@action:inmenu"
msgid "Settings"
msgstr "Preferencias"

#: src/contents/ui/desktop/HistoryPage.qml:30
#, kde-format
msgctxt "@info:placeholder"
msgid "Search history…"
msgstr "Buscar en el historial..."

#: src/contents/ui/desktop/HistoryPage.qml:46
#, kde-format
msgctxt "@info:tooltip"
msgid "Clear all history"
msgstr "Borrar el historial"

#: src/contents/ui/desktop/HistoryPage.qml:83
#, kde-format
msgctxt "@info:placeholder"
msgid "No history yet"
msgstr "Aún no hay historial"

#: src/contents/ui/desktop/Tabs.qml:232 src/contents/ui/Tabs.qml:275
#, kde-format
msgctxt "@label"
msgid "Reader mode: %1"
msgstr "Modo lector: %1"

#: src/contents/ui/desktop/Tabs.qml:261 src/contents/ui/Tabs.qml:306
#, kde-format
msgctxt "@info:tooltip"
msgid "Close tab"
msgstr "Cerrar la pestaña"

#: src/contents/ui/desktop/Tabs.qml:291
#, kde-format
msgctxt "@info:tooltip"
msgid "Open a new tab"
msgstr "Abrir una nueva pestaña"

#: src/contents/ui/desktop/Tabs.qml:324
#, kde-format
msgctxt "@info:tooltip"
msgid "List all tabs"
msgstr "Listar todas las pestañas"

#: src/contents/ui/desktop/Tabs.qml:364
#, kde-format
msgctxt "@action:inmenu"
msgid "Reload Tab"
msgstr "Volver a cargar la pestaña"

#: src/contents/ui/desktop/Tabs.qml:372
#, kde-format
msgctxt "@action:inmenu"
msgid "Duplicate Tab"
msgstr "Duplicar la pestaña"

#: src/contents/ui/desktop/Tabs.qml:380
#, kde-format
msgctxt "@action:inmenu"
msgid "Bookmark Tab"
msgstr "Marcar la pestaña"

#: src/contents/ui/desktop/Tabs.qml:396
#, kde-format
msgctxt "@action:inmenu"
msgid "Close Tab"
msgstr "Cerrar la pestaña"

#: src/contents/ui/Downloads.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Downloads"
msgstr "Descargas"

#: src/contents/ui/Downloads.qml:28
#, kde-format
msgctxt "@info:placeholder"
msgid "No running downloads"
msgstr "No se está descargando nada"

#: src/contents/ui/Downloads.qml:77
#, kde-format
msgctxt "@info:progress"
msgid "Starting…"
msgstr "Iniciando…"

#: src/contents/ui/Downloads.qml:79
#, kde-format
msgctxt "@info:progress"
msgid "Completed"
msgstr "Completa"

#: src/contents/ui/Downloads.qml:81
#, kde-format
msgctxt "@info:progress"
msgid "Cancelled"
msgstr "Cancelada"

#: src/contents/ui/Downloads.qml:83
#, kde-format
msgctxt "@info:progress"
msgid "Interrupted"
msgstr "Interrumpida"

#: src/contents/ui/Downloads.qml:85
#, kde-format
msgctxt "@info:progress"
msgid "In progress…"
msgstr "En curso..."

#: src/contents/ui/Downloads.qml:92
#, kde-format
msgctxt "@info:tooltip"
msgid "Cancel"
msgstr "Cancelar"

#: src/contents/ui/Downloads.qml:117
#, kde-format
msgctxt "@info:tooltip"
msgid "Continue"
msgstr "Continuar"

#: src/contents/ui/FindInPageBar.qml:45
#, kde-format
msgctxt "@info:placeholder"
msgid "Search…"
msgstr "Buscar..."

#: src/contents/ui/History.qml:12
#, kde-format
msgctxt "@title:window"
msgid "History"
msgstr "Historial"

#: src/contents/ui/mobile.qml:20
#, kde-format
msgctxt "@title:window"
msgid "Angelfish Web Browser"
msgstr "Navegador web Angelfish"

#: src/contents/ui/mobile.qml:66
#, kde-format
msgctxt "@action:inmenu"
msgid "Tabs"
msgstr "Pestañas"

#: src/contents/ui/mobile.qml:307
#, kde-format
msgctxt "@action:inmenu"
msgid "Add to Homescreen"
msgstr "Añadir a la pantalla de inicio"

#: src/contents/ui/mobile.qml:317
#, kde-format
msgctxt "@action:inmenu"
msgid "Open in App"
msgstr "Abrir en la aplicación"

#: src/contents/ui/mobile.qml:325
#, kde-format
msgctxt "@action:inmenu"
msgid "Go Back"
msgstr "Volver"

#: src/contents/ui/mobile.qml:333
#, kde-format
msgctxt "@action:inmenu"
msgid "Go Forward"
msgstr "Avanzar"

#: src/contents/ui/mobile.qml:340
#, kde-format
msgctxt "@action:inmenu"
msgid "Stop Loading"
msgstr "Detener la carga"

#: src/contents/ui/mobile.qml:340
#, kde-format
msgctxt "@action:inmenu"
msgid "Refresh"
msgstr "Actualizar"

#: src/contents/ui/mobile.qml:350
#, kde-format
msgctxt "@info:status"
msgid "Bookmarked"
msgstr "Marcada"

#: src/contents/ui/mobile.qml:350
#, kde-format
msgctxt "@action:inmenu"
msgid "Bookmark"
msgstr "Marcador"

#: src/contents/ui/mobile.qml:366
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Desktop Site"
msgstr "Mostrar el sitio del escritorio"

#: src/contents/ui/mobile.qml:383
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Navigation Bar"
msgstr "Ocultar la barra de navegación"

#: src/contents/ui/Navigation.qml:410
#, kde-format
msgctxt "@action:intoolbar"
msgid "Done"
msgstr "Terminado"

#: src/contents/ui/Navigation.qml:423
#, kde-format
msgctxt "@info:status"
msgid "%1 tabs"
msgstr "%1 pestañas"

#: src/contents/ui/NewTabQuestion.qml:11
#, kde-format
msgctxt "@info:status"
msgid ""
"Site wants to open a new tab:\n"
"%1"
msgstr ""
"El sitio quiere abrir una nueva pestaña:\n"
"%1"

#: src/contents/ui/NewTabQuestion.qml:19
#, kde-format
msgctxt "@action:inmenu"
msgid "Open"
msgstr "Abrir"

#: src/contents/ui/ShareSheet.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Share Page"
msgstr "Compartir página"

#: src/main.cpp:82
#, kde-format
msgid "URL to open"
msgstr "URL que se abrirá"

#: src/settings/AngelfishConfigurationView.qml:14
#: src/settings/SettingsGeneral.qml:18
#, kde-format
msgctxt "@title:window"
msgid "General"
msgstr "General"

#: src/settings/AngelfishConfigurationView.qml:20
#, kde-format
msgctxt "@title:window"
msgid "Adblock"
msgstr "Bloqueo de publicidad"

#: src/settings/AngelfishConfigurationView.qml:26
#: src/settings/SettingsWebApps.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Web Apps"
msgstr "Aplicaciones web"

#: src/settings/AngelfishConfigurationView.qml:32
#: src/settings/SettingsSearchEnginePage.qml:112
#, kde-format
msgctxt "@title:window"
msgid "Search Engines"
msgstr "Motores de búsqueda"

#: src/settings/AngelfishConfigurationView.qml:38
#: src/settings/DesktopHomeSettingsPage.qml:15
#, kde-format
msgctxt "@title:window"
msgid "Toolbars"
msgstr "Barras de herramientas"

#: src/settings/DesktopHomeSettingsPage.qml:24
#, kde-format
msgctxt "@label"
msgid "Show home button:"
msgstr "Mostrar el botón de inicio:"

#: src/settings/DesktopHomeSettingsPage.qml:25
#, kde-format
msgid "The home button will be shown next to the reload button in the toolbar."
msgstr ""
"El botón de inicio se mostrará junto al botón de recarga en la barra de "
"herramientas."

#: src/settings/DesktopHomeSettingsPage.qml:35
#, kde-format
msgctxt "@label"
msgid "Homepage:"
msgstr "Página de inicio:"

#: src/settings/DesktopHomeSettingsPage.qml:58
#, kde-format
msgctxt "@label"
msgid "New tabs:"
msgstr "Nuevas pestañas:"

#: src/settings/DesktopHomeSettingsPage.qml:81
#, kde-format
msgctxt "@label:checkbox"
msgid "Always show the tab bar"
msgstr "Mostrar siempre la barra de pestañas"

#: src/settings/DesktopHomeSettingsPage.qml:82
#, kde-format
msgid "The tab bar will be displayed even if there is only one tab open."
msgstr ""
"La barra de pestañas se mostrará incluso si solo hay una pestaña abierta."

#: src/settings/SettingsAdblock.qml:16
#, kde-format
msgctxt "@title:window"
msgid "Adblock Settings"
msgstr "Preferencias del bloqueo de publicidad"

#: src/settings/SettingsAdblock.qml:23
#, kde-format
msgctxt "@info:tooltip"
msgid "Add filter list"
msgstr "Añadir lista de filtrado"

#: src/settings/SettingsAdblock.qml:27
#, kde-format
msgctxt "@action:intoolbar"
msgid "Update Lists"
msgstr "Actualizar listas"

#: src/settings/SettingsAdblock.qml:49
#, kde-format
msgid "The adblock functionality isn't included in this build."
msgstr ""
"La funcionalidad de bloqueo de publicidad no se ha incluido en esta versión."

#: src/settings/SettingsAdblock.qml:100
#, kde-format
msgctxt "@info:tooltip"
msgid "Remove this filter list"
msgstr "Eliminar esta lista de filtrado"

#: src/settings/SettingsAdblock.qml:112
#, kde-format
msgctxt "@action:button"
msgid "Add Filter List"
msgstr "Añadir lista de filtrado"

#: src/settings/SettingsAdblock.qml:124
#, kde-format
msgctxt "@title:group"
msgid "Add Filter List"
msgstr "Añadir lista de filtrado"

#: src/settings/SettingsAdblock.qml:129
#, kde-format
msgctxt "@label"
msgid "Name"
msgstr "Nombre"

#: src/settings/SettingsAdblock.qml:138
#, kde-format
msgctxt "@label"
msgid "URL"
msgstr "URL"

#: src/settings/SettingsGeneral.qml:27
#, kde-format
msgid "Enable JavaScript"
msgstr "Activar JavaScript"

#: src/settings/SettingsGeneral.qml:28
#, kde-format
msgid "This may be required on certain websites for them to work."
msgstr "Esto puede ser necesario para que ciertos sitios web funcionen."

#: src/settings/SettingsGeneral.qml:37
#, kde-format
msgctxt "@label:checkbox"
msgid "Load images"
msgstr "Cargar imágenes"

#: src/settings/SettingsGeneral.qml:38
#, kde-format
msgid "Whether to load images on websites."
msgstr "Si se deben cargar o no las imágenes de los sitios web."

#: src/settings/SettingsGeneral.qml:47
#, kde-format
msgctxt "@label:checkbox"
msgid "Enable adblock"
msgstr "Activar el bloqueo de publicidad"

#: src/settings/SettingsGeneral.qml:48
#, kde-format
msgid "Attempts to prevent advertisements on websites from showing."
msgstr "Intenta impedir que se muestre la publicidad de los sitios web."

#: src/settings/SettingsGeneral.qml:48
#, kde-format
msgid "Adblock functionality was not included in this build."
msgstr ""
"La funcionalidad de bloqueo de publicidad no se ha incluido en esta versión."

#: src/settings/SettingsGeneral.qml:58
#, kde-format
msgctxt "@label:checkbox"
msgid "Switch to new tab immediately"
msgstr "Cambiar a la nueva pestaña inmediatamente"

#: src/settings/SettingsGeneral.qml:59
#, kde-format
msgid ""
"When you open a link, image or media in a new tab, switch to it immediately."
msgstr ""
"Al abrir un enlace, imagen o multimedia en una nueva pestaña, cambiar a ella "
"inmediatamente."

#: src/settings/SettingsGeneral.qml:68
#, kde-format
msgctxt "@label:checkbox"
msgid "Use smooth scrolling"
msgstr "Usar desplazamiento suave"

#: src/settings/SettingsGeneral.qml:69
#, kde-format
msgid ""
"Scrolling is smoother and will not stop suddenly when you stop scrolling. "
"Requires app restart to take effect."
msgstr ""
"El desplazamiento es más suave y no se detendrá bruscamente cuando deje de "
"desplazarse. Necesita reiniciar la aplicación para que tenga efecto."

#: src/settings/SettingsGeneral.qml:78
#, kde-format
msgctxt "@label:checkbox"
msgid "Use dark color scheme"
msgstr "Usar el esquema de color oscuro"

#: src/settings/SettingsGeneral.qml:79
#, kde-format
msgid ""
"Websites will have their color schemes set to dark. Requires app restart to "
"take effect."
msgstr ""
"Los sitios web tendrán un esquema de color oscuro. Necesita reiniciar la "
"aplicación para que tenga efecto."

#: src/settings/SettingsNavigationBarPage.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Navigation Bar"
msgstr "Barra de navegación"

#: src/settings/SettingsNavigationBarPage.qml:26
#, kde-format
msgid ""
"Choose the buttons enabled in navigation bar. Some of the buttons can be "
"hidden only in portrait orientation of the browser and are always shown if "
"the browser is wider than its height.\n"
"\n"
" Note that if you disable the menu buttons, you will be able to access the "
"menus either by swiping from the left or right side or to a side along the "
"bottom of the window."
msgstr ""
"Escoja los botones activos en la barra de navegación. Algunos de ellos se "
"pueden ocultar solo con la orientación de retrato del navegador y se "
"muestran cuando el navegador es más ancho que alto.\n"
"\n"
"Tenga en cuenta que si desactiva los botones del menú podrá acceder al menú "
"deslizando el dedo desde la derecha, desde la izquierda o a un lado desde la "
"parte inferior de la ventana."

#: src/settings/SettingsNavigationBarPage.qml:35
#, kde-format
msgctxt "@label:checkbox"
msgid "Main menu in portrait"
msgstr "Menú principal en modo retrato"

#: src/settings/SettingsNavigationBarPage.qml:41
#, kde-format
msgctxt "@label:checkbox"
msgid "Tabs in portrait"
msgstr "Pestañas en modo retrato"

#: src/settings/SettingsNavigationBarPage.qml:47
#, kde-format
msgctxt "@label:checkbox"
msgid "Context menu in portrait"
msgstr "Menú de contexto en modo retrato"

#: src/settings/SettingsNavigationBarPage.qml:53
#, kde-format
msgctxt "@label:checkbox"
msgid "Go back"
msgstr "Volver"

#: src/settings/SettingsNavigationBarPage.qml:59
#, kde-format
msgctxt "@label:checkbox"
msgid "Go forward"
msgstr "Avanzar"

#: src/settings/SettingsNavigationBarPage.qml:65
#, kde-format
msgctxt "@label:checkbox"
msgid "Reload/Stop"
msgstr "Volver a cargar o detener"

#: src/settings/SettingsSearchEnginePage.qml:17
#, kde-format
msgctxt "@window:title"
msgid "Search Engines"
msgstr "Motores de búsqueda"

#: src/settings/SettingsSearchEnginePage.qml:20
#, kde-format
msgctxt "@label"
msgid "Custom"
msgstr "Personalizar"

#: src/settings/SettingsSearchEnginePage.qml:117
#, kde-format
msgctxt "@label"
msgid "Base URL of your preferred search engine"
msgstr "URL base del motor de búsqueda preferido"

#: src/settings/SettingsWebApps.qml:73
#, kde-format
msgctxt "@info:tooltip"
msgid "Remove app"
msgstr "Eliminar aplicación"

#: src/settings/SettingsWebApps.qml:85
#, kde-format
msgctxt "@info:placeholder"
msgid "No web apps installed"
msgstr "No hay aplicaciones web instaladas"

#~ msgid "Fullscreen"
#~ msgstr "Pantalla completa"

#~ msgid "Private mode"
#~ msgstr "Modo privado"

#~ msgid "Search..."
#~ msgstr "Buscar..."

#~ msgid "Go previous"
#~ msgstr "Retroceder"

#~ msgid "Reader Mode"
#~ msgstr "Modo lector"

#~ msgid "Share page to"
#~ msgstr "Compartir página con"

#~ msgctxt "@action:intoolbar"
#~ msgid "Add Filterlist"
#~ msgstr "Añadir lista de filtrado"

#~ msgid "Url"
#~ msgstr "URL"

#~ msgid "Show navigation bar"
#~ msgstr "Mostrar la barra de navegación"

#~ msgid "Configure Angelfish"
#~ msgstr "Configurar Angelfish"

#~ msgid "Add"
#~ msgstr "Añadir"

#~ msgid "Enabled"
#~ msgstr "Activado"

#~ msgid "Disabled"
#~ msgstr "Desactivado"

#~ msgid "Search online"
#~ msgstr "Buscar en línea"

#~ msgid "Confirm"
#~ msgstr "Confirmar"

#~ msgid "OK"
#~ msgstr "Aceptar"

#~ msgid "General:"
#~ msgstr "General:"

#~ msgid "Enable Adblock"
#~ msgstr "Activar el bloqueo publicitario"

#~ msgid "Home"
#~ msgstr "Página de inicio"

#~ msgid "Adblock filter lists"
#~ msgstr "Listas de filtrado de bloqueo publicitario"

#~ msgid "New"
#~ msgstr "Nueva"

#~ msgid "Choose the buttons enabled in navigation bar. "
#~ msgstr "Escoja los botones activos en la barra de navegación."

#~ msgid "Website that should be loaded on startup"
#~ msgstr "Sitio web que se debe cargar durante el inicio"

#~ msgid "Find..."
#~ msgstr "Buscar..."

#~ msgid "Highlight text on the current website"
#~ msgstr "Resaltar texto en el sitio web actual"

#~ msgid "Start without UI"
#~ msgstr "Iniciar sin interfaz de usuario"

#~ msgid "geo location"
#~ msgstr "geolocalización"

#~ msgid "the microphone"
#~ msgstr "el micrófono"

#~ msgid "the camera"
#~ msgstr "la cámara"

#~ msgid "camera and microphone"
#~ msgstr "la cámara y el micrófono"

#~ msgid "Ok"
#~ msgstr "Aceptar"
